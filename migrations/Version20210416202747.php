<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210416202747 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE historic_question_entity (id INT AUTO_INCREMENT NOT NULL, question_entity_id INT NOT NULL, field VARCHAR(255) NOT NULL, value VARCHAR(255) NOT NULL, INDEX IDX_250A5A6588F039C6 (question_entity_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE historic_question_entity ADD CONSTRAINT FK_250A5A6588F039C6 FOREIGN KEY (question_entity_id) REFERENCES question_entity (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE historic_question_entity');
    }
}
