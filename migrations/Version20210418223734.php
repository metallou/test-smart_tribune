<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210418223734 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE answer_entity DROP FOREIGN KEY FK_FCD17ECF88F039C6');
        $this->addSql('DROP INDEX IDX_FCD17ECF88F039C6 ON answer_entity');
        $this->addSql('ALTER TABLE answer_entity CHANGE question_entity_id question_id INT NOT NULL');
        $this->addSql('ALTER TABLE answer_entity ADD CONSTRAINT FK_FCD17ECF1E27F6BF FOREIGN KEY (question_id) REFERENCES question_entity (id)');
        $this->addSql('CREATE INDEX IDX_FCD17ECF1E27F6BF ON answer_entity (question_id)');
        $this->addSql('ALTER TABLE historic_question_entity DROP FOREIGN KEY FK_250A5A6588F039C6');
        $this->addSql('DROP INDEX IDX_250A5A6588F039C6 ON historic_question_entity');
        $this->addSql('ALTER TABLE historic_question_entity CHANGE question_entity_id question_id INT NOT NULL');
        $this->addSql('ALTER TABLE historic_question_entity ADD CONSTRAINT FK_250A5A651E27F6BF FOREIGN KEY (question_id) REFERENCES question_entity (id)');
        $this->addSql('CREATE INDEX IDX_250A5A651E27F6BF ON historic_question_entity (question_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE answer_entity DROP FOREIGN KEY FK_FCD17ECF1E27F6BF');
        $this->addSql('DROP INDEX IDX_FCD17ECF1E27F6BF ON answer_entity');
        $this->addSql('ALTER TABLE answer_entity CHANGE question_id question_entity_id INT NOT NULL');
        $this->addSql('ALTER TABLE answer_entity ADD CONSTRAINT FK_FCD17ECF88F039C6 FOREIGN KEY (question_entity_id) REFERENCES question_entity (id)');
        $this->addSql('CREATE INDEX IDX_FCD17ECF88F039C6 ON answer_entity (question_entity_id)');
        $this->addSql('ALTER TABLE historic_question_entity DROP FOREIGN KEY FK_250A5A651E27F6BF');
        $this->addSql('DROP INDEX IDX_250A5A651E27F6BF ON historic_question_entity');
        $this->addSql('ALTER TABLE historic_question_entity CHANGE question_id question_entity_id INT NOT NULL');
        $this->addSql('ALTER TABLE historic_question_entity ADD CONSTRAINT FK_250A5A6588F039C6 FOREIGN KEY (question_entity_id) REFERENCES question_entity (id)');
        $this->addSql('CREATE INDEX IDX_250A5A6588F039C6 ON historic_question_entity (question_entity_id)');
    }
}
