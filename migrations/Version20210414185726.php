<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210414185726 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE answer_entity (id INT AUTO_INCREMENT NOT NULL, question_entity_id INT NOT NULL, body VARCHAR(500) NOT NULL, channel VARCHAR(3) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_FCD17ECF88F039C6 (question_entity_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE question_entity (id INT AUTO_INCREMENT NOT NULL, promoted TINYINT(1) NOT NULL, status VARCHAR(9) NOT NULL, title VARCHAR(100) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE answer_entity ADD CONSTRAINT FK_FCD17ECF88F039C6 FOREIGN KEY (question_entity_id) REFERENCES question_entity (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE answer_entity DROP FOREIGN KEY FK_FCD17ECF88F039C6');
        $this->addSql('DROP TABLE answer_entity');
        $this->addSql('DROP TABLE question_entity');
    }
}
