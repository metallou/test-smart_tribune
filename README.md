# test Smart Tribune

## How to install

### Requirements
* php 7.4
* symfony CLI
* composer
* sql database

### Steps
1. Clone repository: `git clone https://gitlab.com/metallou/test-smart_tribune.git`
2. `cd test-smart_tribune`
3. If any parameter defined in `.env` does not satisfy your needs, copy `.env` file as `.env.local` and modify this new file accordingly
4. Install dependencies: `symfony composer install`
5. Run database migrations: `symfony php bin/console doctrine:migrations:migrate`
6. Run server: `symfony serve`

## How to use

### base URL
API will be available at: `http://localhost:8080` and `https://localhost:8080`

### routes
All routes require the same body
```typescript
interface AnswerInterface {
	channel: 'bot' | 'faq';
	body: string:
}
interface QuestionInterface {
	answers: Array<AnswerInterface>,
	promoted: boolean;
	status: 'draft' | 'published';
	title: string;
}
```
With sereval requirements:
* answers: at least 1 element
* answer.body: at most 500 characters
* title: at most 100 characters

Example:
```json
{
	"title" : "title",
	"promoted": false,
	"status": "draft",
	"answers": [
		{
			"body": "body",
			"channel": "bot"
		}
	]
}
```

1. `POST /questions`: create a question
2. `PUT /questions/:id`: update some fields of a question

## How to test

### Requirements
A secondary database is needed for tests

### Steps
1. If any parameter defined in `.env.test` does not satisfy your needs, copy `.env.test` file as `.env.test.local` and modify this new file accordingly
2. Destroy previous database: `symfony php bin/console --env=test --if-exists --force -- doctrine:database:drop`
3. Create database: `symfony php bin/console --env=test -- doctrine:database:create`
4. Setup database structure: `symfony php bin/console --env=test -- doctrine:schema:create`
5. Run tests: `symfony php bin/phpunit -vvv`

## Going further
* To populate HistoricQuestion asynchronously, use symfony/messenger package and replace event based logic with message based logic
* Unfortunately, I cannot run docker at home
