<?php

namespace Test\Event;

use App\Event\UpdateEvent;
use PHPUnit\Framework\TestCase;
use Test\EntityFactory;
use Test\EventFactory;

/**
 * @covers \App\Event\UpdateEvent
 */
final class UpdateEventTest extends TestCase
{
    public function testQuestionEntity() : void
    {
        // arrange
        $questionEntity = EntityFactory::question();
        $systemUnderTest = EventFactory::update(
            [
                'question_entity' => $questionEntity,
            ]
        );

        // act
        $output = $systemUnderTest->getQuestionEntity();

        // assert
        self::assertSame(
            $questionEntity,
            $output,
        );
    }

    public function testField() : void
    {
        // arrange
        $field = 'field';
        $systemUnderTest = EventFactory::update(
            [
                'field' => $field,
            ]
        );

        // act
        $output = $systemUnderTest->getField();

        // assert
        self::assertSame(
            $field,
            $output,
        );
    }

    public function testValue() : void
    {
        // arrange
        $value = 'value';
        $systemUnderTest = EventFactory::update(
            [
                'value' => $value,
            ]
        );

        // act
        $output = $systemUnderTest->getValue();

        // assert
        self::assertSame(
            $value,
            $output,
        );
    }
}
