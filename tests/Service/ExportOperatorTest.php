<?php

namespace Test\Service;

use App\Entity\HistoricQuestionEntity;
use App\Service\ExportOperator;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Serializer\SerializerInterface;
use Test\EntityFactory;
use Test\KernelTestCaseAbstract;

/**
 * @covers \App\Service\ExportOperator
 */
final class ExportOperatorTest extends KernelTestCaseAbstract
{
    private SerializerInterface $mockSerializer;

    public function testExport() : void {
        // arrange
        $systemUnderTest = $this->createSUT();

        $queryBuilder = new QueryBuilder(
            $this->entityManager
        );
        $queryBuilder
            ->select('HistoricQuestion')
            ->from(
                HistoricQuestionEntity::class,
                'HistoricQuestion',
            );

        $this->persist(
            EntityFactory::historicQuestion(),
        );

        // expect
        $this->mockSerializer
            ->expects(self::once())
            ->method('serialize')
            ->with(
                self::isType('array'),
                self::equalTo('csv'),
                self::isType('array'),
            )
            ->willReturn('csv');

        // act
        $systemUnderTest->export(
            $queryBuilder,
            [],
            static function() : array {
                return [];
            },
        );
    }

    private function createSUT() : ExportOperator
    {
        return new ExportOperator(
            $this->mockSerializer,
        );
    }

    protected function setUp() : void
    {
        parent::setUp();

        $this->mockSerializer = $this->createMock(SerializerInterface::class);
    }
}