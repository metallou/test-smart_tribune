<?php

namespace Test\Service\Controller;

use App\DataTransferObject\QuestionDataTransferObject;
use App\Entity\QuestionEntity;
use App\Event\UpdateEvent;
use App\Exception\ControllerException;
use App\Repository\HistoricQuestionEntityRepository;
use App\Service\Controller\MainControllerService;
use App\Service\DataTransferObjectService;
use App\Service\ExportOperator;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use PHPUnit\Framework\TestCase;
use Psr\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Test\EntityFactory;
use Test\TestContainer;

/**
 * @covers \App\Service\Controller\MainControllerService
 */
class MainControllerServiceTest extends TestCase
{
    public function testCreateThrowsWithInvalidPayload() : void
    {
        // arrange
        $systemUnderTest = $this->createSUT();

        $mockDataTransferObjectService = $this->createMock(DataTransferObjectService::class);
        $mockEntityManager             = $this->createMock(EntityManagerInterface::class);
        $request                       = self::createRequest(null);

        $constraintViolationList = $this->createConstraintsViolationList(true);

        // expect
        $this->expectException(ControllerException::class);
        $this->expectExceptionMessage('invalid payload');
        $mockDataTransferObjectService
            ->expects(self::once())
            ->method('validateProperty')
            ->with(
                self::isInstanceOf(QuestionDataTransferObject::class),
                self::equalto('payload'),
            )
            ->willReturn($constraintViolationList);

        // act
        $systemUnderTest->create(
            $mockDataTransferObjectService,
            $mockEntityManager,
            $request,
        );
    }

    public function testCreateThrowsWithInvalidData() : void
    {
        // arrange
        $systemUnderTest = $this->createSUT();

        $mockDataTransferObjectService = $this->createMock(DataTransferObjectService::class);
        $mockEntityManager             = $this->createMock(EntityManagerInterface::class);
        $request                       = self::createRequest('{}');

        $constraintViolationListEmpty = $this->createConstraintsViolationList(false);
        $constraintViolationList      = $this->createConstraintsViolationList(true);

        // expect
        $this->expectException(ControllerException::class);
        $this->expectExceptionMessage('invalid data');
        $mockDataTransferObjectService
            ->expects(self::exactly(2))
            ->method('validateProperty')
            ->withConsecutive(
                [
                    self::isInstanceOf(QuestionDataTransferObject::class),
                    self::equalto('payload'),
                ],
                [
                    self::isInstanceOf(QuestionDataTransferObject::class),
                    self::equalto('data'),
                ],
            )
            ->willReturnOnConsecutiveCalls(
                $constraintViolationListEmpty,
                $constraintViolationList,
            );

        // act
        $systemUnderTest->create(
            $mockDataTransferObjectService,
            $mockEntityManager,
            $request,
        );
    }

    public function testCreate() : void
    {
        // arrange
        $systemUnderTest = $this->createSUT();

        $data = [
            'answers' => [
                [
                    'channel' => 'channel',
                    'body'    => 'body',
                ],
            ],
            'promoted' => true,
            'status'   => 'status',
            'title'    => 'title',
        ];
        $json = json_encode($data);

        $mockDataTransferObjectService = $this->createMock(DataTransferObjectService::class);
        $mockEntityManager             = $this->createMock(EntityManagerInterface::class);
        $request                       = self::createRequest($json);

        $constraintViolationListEmpty = $this->createConstraintsViolationList(false);

        // expect
        $mockDataTransferObjectService
            ->expects(self::exactly(2))
            ->method('validateProperty')
            ->withConsecutive(
                [
                    self::isInstanceOf(QuestionDataTransferObject::class),
                    self::equalto('payload'),
                ],
                [
                    self::isInstanceOf(QuestionDataTransferObject::class),
                    self::equalto('data'),
                ],
            )
            ->willReturnOnConsecutiveCalls(
                $constraintViolationListEmpty,
                $constraintViolationListEmpty,
            );
        $mockEntityManager
            ->expects(self::once())
            ->method('persist')
            ->with(
                self::isInstanceOf(QuestionEntity::class),
            );
        $mockEntityManager
            ->expects(self::once())
            ->method('flush');

        // act
        $output = $systemUnderTest->create(
            $mockDataTransferObjectService,
            $mockEntityManager,
            $request,
        );

        // assert
        self::assertCount(
            1,
            $output,
        );
        self::assertArrayHasKey(
            'data',
            $output,
        );
        self::assertInstanceOf(
            QuestionEntity::class,
            $output['data'],
        );
    }

    public function testUpdateThrowsWithInvalidPayload() : void
    {
        // arrange
        $systemUnderTest = $this->createSUT();

        $mockDataTransferObjectService = $this->createMock(DataTransferObjectService::class);
        $mockEntityManager             = $this->createMock(EntityManagerInterface::class);
        $mockEventDispatcher           = $this->createMock(EventDispatcherInterface::class);
        $questionEntity                = EntityFactory::question();
        $request                       = self::createRequest(null);

        $constraintViolationList = $this->createConstraintsViolationList(true);

        // expect
        $this->expectException(ControllerException::class);
        $this->expectExceptionMessage('invalid payload');
        $mockDataTransferObjectService
            ->expects(self::once())
            ->method('validateProperty')
            ->with(
                self::isInstanceOf(QuestionDataTransferObject::class),
                self::equalto('payload'),
            )
            ->willReturn($constraintViolationList);

        // act
        $systemUnderTest->update(
            $mockDataTransferObjectService,
            $mockEntityManager,
            $mockEventDispatcher,
            $questionEntity,
            $request,
        );
    }

    public function testUpdateThrowsWithInvalidData() : void
    {
        // arrange
        $systemUnderTest = $this->createSUT();

        $mockDataTransferObjectService = $this->createMock(DataTransferObjectService::class);
        $mockEntityManager             = $this->createMock(EntityManagerInterface::class);
        $mockEventDispatcher           = $this->createMock(EventDispatcherInterface::class);
        $questionEntity                = EntityFactory::question();
        $request                       = self::createRequest('{}');

        $constraintViolationListEmpty = $this->createConstraintsViolationList(false);
        $constraintViolationList      = $this->createConstraintsViolationList(true);

        // expect
        $this->expectException(ControllerException::class);
        $this->expectExceptionMessage('invalid data');
        $mockDataTransferObjectService
            ->expects(self::exactly(2))
            ->method('validateProperty')
            ->withConsecutive(
                [
                    self::isInstanceOf(QuestionDataTransferObject::class),
                    self::equalto('payload'),
                ],
                [
                    self::isInstanceOf(QuestionDataTransferObject::class),
                    self::equalto('data'),
                ],
            )
            ->willReturnOnConsecutiveCalls(
                $constraintViolationListEmpty,
                $constraintViolationList,
            );

        // act
        $systemUnderTest->update(
            $mockDataTransferObjectService,
            $mockEntityManager,
            $mockEventDispatcher,
            $questionEntity,
            $request,
        );
    }

    public function testUpdate() : void
    {
        // arrange
        $systemUnderTest = $this->createSUT();

        $data = [
            'answers' => [
                [
                    'channel' => 'channel',
                    'body'    => 'body',
                ],
            ],
            'promoted' => true,
            'status'   => 'newStatus',
            'title'    => 'newTitle',
        ];
        $json = json_encode($data);

        $mockDataTransferObjectService = $this->createMock(DataTransferObjectService::class);
        $mockEntityManager             = $this->createMock(EntityManagerInterface::class);
        $mockEventDispatcher           = $this->createMock(EventDispatcherInterface::class);
        $questionEntity                = EntityFactory::question(
            [
                'status' => 'oldStatus',
                'title'  => 'oldTitle',
            ]
        );
        $request                       = self::createRequest($json);

        $constraintViolationListEmpty = $this->createConstraintsViolationList(false);

        // expect
        $mockDataTransferObjectService
            ->expects(self::exactly(2))
            ->method('validateProperty')
            ->withConsecutive(
                [
                    self::isInstanceOf(QuestionDataTransferObject::class),
                    self::equalto('payload'),
                ],
                [
                    self::isInstanceOf(QuestionDataTransferObject::class),
                    self::equalto('data'),
                ],
            )
            ->willReturnOnConsecutiveCalls(
                $constraintViolationListEmpty,
                $constraintViolationListEmpty,
            );
        $mockEventDispatcher
            ->expects(self::exactly(2))
            ->method('dispatch')
            ->with(
                self::isInstanceOf(UpdateEvent::class),
            );
        $mockEntityManager
            ->expects(self::once())
            ->method('persist')
            ->with(
                self::equalTo($questionEntity),
            );
        $mockEntityManager
            ->expects(self::once())
            ->method('flush');

        // act
        $output = $systemUnderTest->update(
            $mockDataTransferObjectService,
            $mockEntityManager,
            $mockEventDispatcher,
            $questionEntity,
            $request,
        );

        // assert
        self::assertCount(
            1,
            $output,
        );
        self::assertArrayHasKey(
            'data',
            $output,
        );
        self::assertInstanceOf(
            QuestionEntity::class,
            $output['data'],
        );
    }

    public function testExport() : void
    {
        // arrange
        $systemUnderTest = $this->createSUT();

        $mockExportOperator                   = $this->createMock(ExportOperator::class);
        $mockHistoricQuestionEntityRepository = $this->createMock(HistoricQuestionEntityRepository::class);
        $mockQueryBuilder                     = $this->createMock(QueryBuilder::class);

        $file = 'file';

        // expect
        $mockHistoricQuestionEntityRepository
            ->expects(self::once())
            ->method('createQueryBuilder')
            ->with(
                self::equalTo('HistoricQuestion'),
            )
            ->willReturn($mockQueryBuilder);
        $mockQueryBuilder
            ->expects(self::once())
            ->method('orderBy')
            ->with(
                self::equalTo('HistoricQuestion.question')
            )
            ->willReturnSelf();
        $mockExportOperator
            ->expects(self::once())
            ->method('export')
            ->with(
                self::equalTo($mockQueryBuilder),
                self::isType('array'),
                self::isType('callable'),
            )
            ->willReturn($file);

        // act
        $output = $systemUnderTest->export(
            $mockExportOperator,
            $mockHistoricQuestionEntityRepository,
        );

        // assert
        self::assertSame(
            $file,
            $output,
        );
    }

    private function createConstraintsViolationList(
        bool $withData
    ) : ConstraintViolationListInterface {
        $violations = $withData
            ? [
                $this->createMock(ConstraintViolationInterface::class),
            ]
            : [];

        return new ConstraintViolationList(
            $violations,
        );
    }

    private static function createRequest(
        ?string $content
    ) : Request {
        return new Request(
            [],
            [],
            [],
            [],
            [],
            [],
            $content,
        );
    }

    private function createSUT() : MainControllerService
    {
        return new MainControllerService();
    }
}
