<?php

namespace Test\Service;

use App\DataTransferObject\DataTransferObjectInterface;
use App\Service\DataTransferObjectService;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @covers \App\Service\DataTransferObjectService
 */
final class DataTransferObjectServiceTest extends TestCase
{
    private ValidatorInterface $mockValidator;

    public function testValidateProperty() : void {
        // arrange
        $systemUnderTest = $this->createSUT();
        
        $dataTransferObject       = $this->createMock(DataTransferObjectInterface::class);
        $property                 = 'property';
        $groups                   = 'group';
        $constraintsViolationList = $this->createMock(ConstraintViolationListInterface::class);

        // expect
        $this->mockValidator
            ->expects(self::once())
            ->method('validateProperty')
            ->with(
                self::equalTo($dataTransferObject),
                self::equalTo($property),
                self::equalTo($groups),
            )
            ->willReturn($constraintsViolationList);

        // act
        $output = $systemUnderTest->validateProperty(
            $dataTransferObject,
            $property,
            $groups,
        );
    }

    private function createSUT() : DataTransferObjectService
    {
        return new DataTransferObjectService(
            $this->mockValidator,
        );
    }

    protected function setUp() : void
    {
        parent::setUp();

        $this->mockValidator = $this->createMock(ValidatorInterface::class);
    }
}