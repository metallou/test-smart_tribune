<?php

namespace Test;

use App\DataTransferObject\QuestionDataTransferObject;

final class DataTransferObjectFactory
{
    public static function question(
        string $payload
    ): QuestionDataTransferObject {
        return new QuestionDataTransferObject(
            $payload,
        );
    }
}
