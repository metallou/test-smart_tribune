<?php

namespace Test\Repository;

use App\Repository\EntityRepositoryAbstract;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityNotFoundException;
use Test\EntityFactory;
use Test\KernelTestCaseAbstract;

/**
 * @covers \App\Repository\EntityRepositoryAbstract
 */
final class EntityRepositoryAbstractTest extends KernelTestCaseAbstract
{
    public function testGet() : void
    {
        // arrange
        $systemUnderTest = $this->createSUT();

        $questionEntity = EntityFactory::question();

        $this->persist($questionEntity);

        $id = $questionEntity->getId();

        // act
        $output = $systemUnderTest->get($id);

        // assert
        self::assertSame(
            $id,
            $output->getId(),
        );
    }

    public function testGetThrows() : void
    {
        // arrange
        $systemUnderTest = $this->createSUT();

        // expect
        $this->expectException(EntityNotFoundException::class);
        $this->expectExceptionMessage('App\Entity\QuestionEntity(id=0) not found');

        // act
        $systemUnderTest->get(0);
    }

    private function createSUT() : EntityRepositoryAbstract
    {
        return new TestEntityRepositoryAbstract(
            $this->managerRegistry,
        );
    }
}
