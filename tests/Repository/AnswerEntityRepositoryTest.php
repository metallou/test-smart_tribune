<?php

namespace Test\Repository;

use App\Entity\AnswerEntity;
use App\Repository\AnswerEntityRepository;
use Test\KernelTestCaseAbstract;

/**
 * @covers \App\Repository\AnswerEntityRepository
 */
final class AnswerEntityRepositoryTest extends KernelTestCaseAbstract
{
    public function testConstructor() : void
    {
        // act
        $systemUnderTest = $this->createSUT();

        // assert
        self::assertSame(
            AnswerEntity::class,
            $systemUnderTest->getClassName(),
        );
    }

    private function createSUT() : AnswerEntityRepository
    {
        return new AnswerEntityRepository(
            $this->managerRegistry,
        );
    }
}
