<?php

namespace Test\Repository;

use App\Entity\QuestionEntity;
use App\Repository\QuestionEntityRepository;
use Test\KernelTestCaseAbstract;

/**
 * @covers \App\Repository\QuestionEntityRepository
 */
final class QuestionEntityRepositoryTest extends KernelTestCaseAbstract
{
    public function testConstructor() : void
    {
        // act
        $systemUnderTest = $this->createSUT();

        // assert
        self::assertSame(
            QuestionEntity::class,
            $systemUnderTest->getClassName(),
        );
    }

    private function createSUT() : QuestionEntityRepository
    {
        return new QuestionEntityRepository(
            $this->managerRegistry,
        );
    }
}
