<?php

namespace Test\Repository;

use App\Entity\HistoricQuestionEntity;
use App\Repository\HistoricQuestionEntityRepository;
use Test\KernelTestCaseAbstract;

/**
 * @covers \App\Repository\HistoricQuestionEntityRepository
 */
final class HistoricQuestionEntityRepositoryTest extends KernelTestCaseAbstract
{
    public function testConstructor() : void
    {
        // act
        $systemUnderTest = $this->createSUT();

        // assert
        self::assertSame(
            HistoricQuestionEntity::class,
            $systemUnderTest->getClassName(),
        );
    }

    private function createSUT() : HistoricQuestionEntityRepository
    {
        return new HistoricQuestionEntityRepository(
            $this->managerRegistry,
        );
    }
}
