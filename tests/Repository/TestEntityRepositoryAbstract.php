<?php

namespace Test\Repository;

use App\Entity\QuestionEntity;
use App\Repository\EntityRepositoryAbstract;
use Doctrine\Persistence\ManagerRegistry;

final class TestEntityRepositoryAbstract extends EntityRepositoryAbstract
{
    public function __construct(
        ManagerRegistry $registry
    ) {
        parent::__construct(
            $registry,
            QuestionEntity::class,
        );
    }
}
