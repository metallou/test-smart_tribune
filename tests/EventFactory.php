<?php

namespace Test;

use App\Event\UpdateEvent;

final class EventFactory
{
    public static function update(
        array $data = []
    ): UpdateEvent {        
        $field          = $data['field'] ?? DataFactory::string('field');
        $questionEntity = $data['question_entity'] ?? EntityFactory::question();
        $value          = $data['value'] ?? DataFactory::string('value');

        return new UpdateEvent(
            $field,
            $questionEntity,
            $value,
        );
    }
}
