<?php

namespace Test\Exception;

use App\Exception\ControllerException;
use ArrayObject;
use PHPUnit\Framework\TestCase;

/**
 * @covers \App\Exception\ControllerException
 */
final class ControllerExceptionTest extends TestCase
{
    public function testData() : void
    {
        // arrange
        $data = new ArrayObject();

        $systemUnderTest = $this->createSUT($data);

        // act
        $output = $systemUnderTest->getData();

        // assert
        self::assertSame(
            $data,
            $output,
        );
    }

    private function createSUT(
        ArrayObject $data
    ) : ControllerException {
        return new ControllerException(
            'message',
            $data,
        );
    }
}
