<?php

namespace Test;

use Psr\Container\ContainerInterface;
use RuntimeException;

final class TestContainer implements ContainerInterface
{
    private array $values = [];

    public function set(
        string $name,
        $value
    ) : self {
        $this->values[$name] = $value;

        return $this;
    }

    public function has(
        string $name
    ) : bool {
        return array_key_exists(
            $name,
            $this->values
        );
    }

    public function get(
        string $name
    ) {
        if (!$this->has($name)) {
            throw new RuntimeException(
                "'$name' not found in container"
            );
        }

        return $this->values[$name];
    }
}