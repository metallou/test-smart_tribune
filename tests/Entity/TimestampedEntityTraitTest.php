<?php

namespace Test\Entity;

use DateTimeInterface;
use PHPUnit\Framework\TestCase;

/**
 * @covers \App\Entity\TimestampedEntityTrait
 */
final class TimestampedEntityTraitTest extends TestCase
{
    public function testTimestamps() : void
    {
        // arrange
        $systemUnderTest = $this->createSUT();

        // act
        $systemUnderTest->updateTimestamps();

        // assert
        self::assertInstanceOf(
            DateTimeInterface::class,
            $systemUnderTest->getCreatedAt(),
        );
        self::assertInstanceOf(
            DateTimeInterface::class,
            $systemUnderTest->getUpdatedAt(),
        );
    }

    private function createSUT() : TestTimestampedEntityTrait
    {
        return new TestTimestampedEntityTrait();
    }
}
