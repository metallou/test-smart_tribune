<?php

namespace Test\Entity;

use App\Entity\EntityAbstract;
use PHPUnit\Framework\TestCase;

/**
 * @covers \App\Entity\EntityAbstract
 */
final class EntityAbstractTest extends TestCase
{
    public function testId() : void
    {
        // arrange
        $systemUnderTest = $this->createSUT();

        // act
        $output = $systemUnderTest->getId();

        // assert
        self::assertNull(
            $output,
        );
    }

    private function createSUT() : EntityAbstract
    {
        return new TestEntityAbstract();
    }
}
