<?php

namespace Test\Entity;

use PHPUnit\Framework\TestCase;
use Test\EntityFactory;

/**
 * @covers \App\Entity\QuestionEntity
 */
final class QuestionEntityTest extends TestCase
{
    public function testPromoted() : void
    {
        // arrange
        $systemUnderTest = EntityFactory::question(
            [
                'promoted' => true,
            ]
            );

        // act
        $output = $systemUnderTest->isPromoted();

        // assert
        self::assertTrue(
            $output,
        );
    }
    
    public function testStatus() : void
    {
        // arrange
        $status = 'status';
        
        $systemUnderTest = EntityFactory::question(
            [
                'status' => $status,
            ]
        );

        // act
        $output = $systemUnderTest->getStatus();

        // assert
        self::assertSame(
            $status,
            $output,
        );
    }
    
    public function testTitle() : void
    {
        // arrange
        $title = 'title';

        $systemUnderTest = EntityFactory::question(
            [
                'title' => $title,
            ]
        );

        // act
        $output = $systemUnderTest->getTitle();

        // assert
        self::assertSame(
            $title,
            $output,
        );
    }
    
    public function testAnswers() : void
    {
        // arrange
        $answerEntities = [
            EntityFactory::answer(),
        ];

        $systemUnderTest = EntityFactory::question(
            [
                'answers' => $answerEntities,
            ]
        );

        // act
        $output = $systemUnderTest->getAnswers()
            ->toArray();

        // assert
        self::assertSame(
            $answerEntities,
            $output,
        );
    }
    
    public function testHistoricQuestions() : void
    {
        // arrange
        $historicQuestionEntities = [
            EntityFactory::historicQuestion(),
        ];

        $systemUnderTest = EntityFactory::question(
            [
                'historic_questions' => $historicQuestionEntities,
            ]
        );

        // act
        $output = $systemUnderTest->getHistoricQuestions()
            ->toArray();

        // assert
        self::assertSame(
            $historicQuestionEntities,
            $output,
        );
    }
}
