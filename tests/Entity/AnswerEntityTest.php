<?php

namespace Test\Entity;

use PHPUnit\Framework\TestCase;
use Test\EntityFactory;

/**
 * @covers \App\Entity\AnswerEntity
 */
final class AnswerEntityTest extends TestCase
{
    public function testBody() : void
    {
        // arrange
        $body = 'body';
        $systemUnderTest = EntityFactory::answer(
            [
                'body' => $body,
            ]
        );

        // act
        $output = $systemUnderTest->getBody();

        // assert
        self::assertSame(
            $body,
            $output,
        );
    }

    public function testChannel() : void
    {
        // arrange
        $channel = 'channel';
        $systemUnderTest = EntityFactory::answer(
            [
                'channel' => $channel,
            ]
        );

        // act
        $output = $systemUnderTest->getChannel();

        // assert
        self::assertSame(
            $channel,
            $output,
        );
    }

    public function testQuestion() : void
    {
        // arrange
        $questionEntity = EntityFactory::question();
        $systemUnderTest = EntityFactory::answer(
            [
                'question' => $questionEntity,
            ]
        );

        // act
        $output = $systemUnderTest->getQuestion();

        // assert
        self::assertSame(
            $questionEntity,
            $output,
        );
    }
}
