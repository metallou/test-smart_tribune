<?php

namespace Test\Entity;

use PHPUnit\Framework\TestCase;
use Test\EntityFactory;

/**
 * @covers \App\Entity\HistoricQuestionEntity
 */
final class HistoricQuestionEntityTest extends TestCase
{
    public function testField() : void
    {
        // arrange
        $field = 'field';
        $systemUnderTest = EntityFactory::historicQuestion(
            [
                'field' => $field,
            ]
        );

        // act
        $output = $systemUnderTest->getField();

        // assert
        self::assertSame(
            $field,
            $output,
        );
    }

    public function testValue() : void
    {
        // arrange
        $value = 'value';
        $systemUnderTest = EntityFactory::historicQuestion(
            [
                'value' => $value,
            ]
        );

        // act
        $output = $systemUnderTest->getValue();

        // assert
        self::assertSame(
            $value,
            $output,
        );
    }

    public function testQuestion() : void
    {
        // arrange
        $questionEntity = EntityFactory::question();
        $systemUnderTest = EntityFactory::historicQuestion(
            [
                'question' => $questionEntity,
            ]
        );

        // act
        $output = $systemUnderTest->getQuestion();

        // assert
        self::assertSame(
            $questionEntity,
            $output,
        );
    }
}
