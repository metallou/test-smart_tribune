<?php

namespace Test\Entity;

use App\Entity\TimestampedEntityTrait;

final class TestTimestampedEntityTrait
{
    use TimestampedEntityTrait;
}
