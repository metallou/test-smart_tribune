<?php

namespace Test;

use App\Entity\EntityAbstract;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

abstract class KernelTestCaseAbstract extends KernelTestCase
{
    protected ManagerRegistry $managerRegistry;
    protected EntityManagerInterface $entityManager;

    protected function persist(
        EntityAbstract ...$entities
    ) : void {
        foreach($entities as $entity) {
            $this->entityManager->persist($entity);
        }

        $this->entityManager->flush();
    }

    protected function setUp(): void
    {
        parent::setUp();

        $kernel = self::bootKernel();

        $this->managerRegistry = $kernel->getContainer()->get('doctrine');
        $this->entityManager   = $this->managerRegistry->getManager();

        $this->entityManager->beginTransaction();
    }

    protected function tearDown(): void
    {
        $this->entityManager->rollback();
        $this->entityManager->close();

        parent::tearDown();
    }
}
