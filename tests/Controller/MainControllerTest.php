<?php

namespace Test\Controller;

use App\Controller\MainController;
use App\Repository\HistoricQuestionEntityRepository;
use App\Repository\QuestionEntityRepository;
use App\Service\Controller\MainControllerService;
use App\Service\DataTransferObjectService;
use App\Service\ExportOperator;
use ArrayObject;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Psr\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Regex;
use Test\EntityFactory;
use Test\TestContainer;
use org\bovigo\vfs\vfsStream;
use org\bovigo\vfs\vfsStreamDirectory;

/**
 * @covers \App\Controller\MainController
 */
class MainControllerTest extends TestCase
{
    private MainControllerService $mockControllerService;
    private TestContainer $testContainer;
    private vfsStreamDirectory $vfs;

    public function testCreate() : void
    {
        // arrange
        $systemUnderTest = $this->createSUT();

        $dataTransferObjectService = $this->createMock(DataTransferObjectService::class);
        $entityManager             = $this->createMock(EntityManagerInterface::class);
        $request                       = new Request();
        $data                          = new ArrayObject(
            [
                'key' => 'value',
            ]
        );

        // expect
        $this->mockControllerService
            ->expects(self::once())
            ->method('create')
            ->with(
                self::equalTo($dataTransferObjectService),
                self::equalTo($entityManager),
                self::equalTo($request),
            )
            ->willReturn($data);

        // act
        $output = $systemUnderTest->create(
            $dataTransferObjectService,
            $entityManager,
            $request,
        );

        // assert
        self::assertSame(
            201,
            $output->getStatusCode(),
        );
        self::assertSame(
            '{"key":"value"}',
            $output->getContent(),
        );
    }

    public function testUpdate() : void
    {
        // arrange
        $systemUnderTest = $this->createSUT();

        $dataTransferObjectService = $this->createMock(DataTransferObjectService::class);
        $entityManager             = $this->createMock(EntityManagerInterface::class);
        $eventDispatcher           = $this->createMock(EventDispatcherInterface::class);
        $questionEntityRepository  = $this->createMock(QuestionEntityRepository::class);
        $request                       = new Request();
        $data                          = new ArrayObject(
            [
                'key' => 'value',
            ]
        );
        $id = 42;
        $questionEntity = EntityFactory::question();

        // expect
        $questionEntityRepository
            ->expects(self::once())
            ->method('get')
            ->with(
                self::equalTo($id),
            )
            ->willReturn($questionEntity);
        $this->mockControllerService
            ->expects(self::once())
            ->method('update')
            ->with(
                self::equalTo($dataTransferObjectService),
                self::equalTo($entityManager),
                self::equalTo($eventDispatcher),
                self::equalTo($questionEntity),
                self::equalTo($request),
            )
            ->willReturn($data);

        // act
        $output = $systemUnderTest->update(
            $dataTransferObjectService,
            $entityManager,
            $eventDispatcher,
            $questionEntityRepository,
            $request,
            $id,
        );

        // assert
        self::assertSame(
            200,
            $output->getStatusCode(),
        );
        self::assertSame(
            '{"key":"value"}',
            $output->getContent(),
        );
    }

    public function testExport() : void
    {
        // arrange
        $systemUnderTest = $this->createSUT();

        $exportOperator                   = $this->createMock(ExportOperator::class);
        $historicQuestionEntityRepository = $this->createMock(HistoricQuestionEntityRepository::class);

        $file = $this->vfs
            ->getChild('file.csv')
            ->url();

        // expect
        $this->mockControllerService
            ->expects(self::once())
            ->method('export')
            ->with(
                self::equalTo($exportOperator),
                self::equalTo($historicQuestionEntityRepository),
            )
            ->willReturn($file);

        // act
        $output = $systemUnderTest->export(
            $exportOperator,
            $historicQuestionEntityRepository,
        );

        // assert
        self::assertSame(
            200,
            $output->getStatusCode(),
        );
        self::assertSame(
            $file,
            $output->getFile()->getPathname(),
        );
    }

    private function createSUT() : MainController
    {
        $controller = new MainController(
            $this->mockControllerService,
        );
        $controller->setContainer($this->testContainer);

        return $controller;
    }

    protected function setUp() : void
    {
        parent::setUp();

        $this->mockControllerService = $this->createMock(MainControllerService::class);
        $this->testContainer         = new TestContainer();
        $this->vfs = vfsStream::setup(
            'exampleDir',
            null,
            [
                'file.csv' => 'CSV content',
            ]
        );
    }
}
