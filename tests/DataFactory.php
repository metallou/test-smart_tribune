<?php

namespace Test;

final class DataFactory
{
    public static function integer(): int {
        static $integer = -1;

        $integer++;

        return $integer;
    }

    public static function string(
        string $string
    ): string {
        $integer = self::integer();

        return "{$string}{$integer}";
    }
}
