<?php

namespace Test\DataTransferObject;

use App\DataTransferObject\QuestionDataTransferObject;
use PHPUnit\Framework\TestCase;
use Test\DataTransferObjectFactory;

/**
 * @covers \App\DataTransferObject\QuestionDataTransferObject
 */
final class QuestionDataTransferObjectTest extends TestCase
{
    public function testPayload() : void
    {
        // arrange
        $payload = 'payload';

        $systemUnderTest = DataTransferObjectFactory::question($payload);

        // act
        $output = $systemUnderTest->getPayload();

        // assert
        self::assertSame(
            $payload,
            $output,
        );
    }

    public function testEmptyPayload() : void
    {
        // arrange
        $payload = 'payload';

        $systemUnderTest = DataTransferObjectFactory::question($payload);

        // act
        $systemUnderTest->parsePayload();
        $output = $systemUnderTest->getData();

        // assert
        self::assertSame(
            [],
            $output,
        );
    }

    public function testData() : void
    {
        // arrange
        $data = [
            'key' => 'value',
        ];
        $payload = json_encode($data);

        $systemUnderTest = DataTransferObjectFactory::question($payload);

        // act
        $systemUnderTest->parsePayload();
        $output = $systemUnderTest->getData();

        // assert
        self::assertSame(
            $data,
            $output,
        );
    }
}