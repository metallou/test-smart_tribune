<?php

namespace Test\EventSubscriber;

use App\Entity\HistoricQuestionEntity;
use App\Event\UpdateEvent;
use App\EventSubscriber\UpdateEventSubscriber;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Test\EntityFactory;

/**
 * @covers \App\EventSubscriber\UpdateEventSubscriber
 */
final class UpdateEventSubscriberTest extends TestCase
{
    private EntityManagerInterface $mockEntityManager;

    public function testGetSubscribedEvents() : void {
        // act
        $output = UpdateEventSubscriber::getSubscribedEvents();

        // assert
        self::assertCount(
            1,
            $output,
        );
        self::assertArrayHasKey(
            UpdateEvent::class,
            $output,
        );
        self::assertSame(
            UpdateEventSubscriber::CALLABLE,
            $output[UpdateEvent::class],
        );
    }

    public function testOnException() : void
    {
        // arrange
        $systemUnderTest = $this->createSUT();

        $questionEntity = EntityFactory::question();
        $updateEvent = new UpdateEvent(
            'field',
            $questionEntity,
            'value',
        );

        // expect
        $this->mockEntityManager
            ->expects(self::once())
            ->method('persist')
            ->with(
                self::isInstanceOf(HistoricQuestionEntity::class),
            );
        $this->mockEntityManager
            ->expects(self::once())
            ->method('flush');

        // act
        $output = $systemUnderTest->onUpdate($updateEvent);

        // assert
        self::assertSame(
            $updateEvent,
            $output,
        );
    }

    private function createSUT() : UpdateEventSubscriber {
        return new UpdateEventSubscriber(
            $this->mockEntityManager,
        );
    }

    protected function setUp() : void
    {
        parent::setUp();

        $this->mockEntityManager = $this->createMock(EntityManagerInterface::class);
    }
}
