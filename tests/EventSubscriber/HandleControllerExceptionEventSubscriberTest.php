<?php

namespace Test\EventSubscriber;

use App\EventSubscriber\HandleControllerExceptionEventSubscriber;
use App\Exception\ControllerException;
use ArrayObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @covers \App\EventSubscriber\HandleControllerExceptionEventSubscriber
 */
final class HandleControllerExceptionEventSubscriberTest extends TestCase
{
    private SerializerInterface $mockSerializer;

    public function testGetSubscribedEvents() : void {
        // act
        $output = HandleControllerExceptionEventSubscriber::getSubscribedEvents();

        // assert
        self::assertCount(
            1,
            $output,
        );
        self::assertArrayHasKey(
            KernelEvents::EXCEPTION,
            $output,
        );
        self::assertSame(
            HandleControllerExceptionEventSubscriber::CALLABLE,
            $output[KernelEvents::EXCEPTION],
        );
    }

    public function testOnException() : void
    {
        // arrange
        $systemUnderTest = $this->createSUT();

        $data = new ArrayObject();
        $controllerException = new ControllerException(
            'message',
            $data,
        );

        $kernel = $this->createMock(KernelInterface::class);
        $request = new Request();
        $exceptionEvent = new ExceptionEvent(
            $kernel,
            $request,
            0,
            $controllerException,
        );

        $json = '{}';

        // expect
        $this->mockSerializer
            ->expects(self::once())
            ->method('serialize')
            ->with(
                self::equalTo($data),
                self::equalTo('json'),
            )
            ->willReturn($json);

        // act
        $output = $systemUnderTest->onException($exceptionEvent);
        $response = $output->getResponse();

        // assert
        self::assertSame(
            $exceptionEvent,
            $output,
        );
        self::assertInstanceOf(
            JsonResponse::class,
            $response,
        );
        self::assertSame(
            400,
            $response->getStatusCode(),
        );
        self::assertSame(
            $json,
            $response->getContent(),
        );
    }

    private function createSUT() : HandleControllerExceptionEventSubscriber {
        return new HandleControllerExceptionEventSubscriber(
            $this->mockSerializer,
        );
    }

    protected function setUp() : void
    {
        parent::setUp();

        $this->mockSerializer = $this->createMock(SerializerInterface::class);
    }
}