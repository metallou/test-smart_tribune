<?php

namespace Test;

use App\Entity\AnswerEntity;
use App\Entity\HistoricQuestionEntity;
use App\Entity\QuestionEntity;

final class EntityFactory
{
    public static function answer(
        array $data = []
    ): AnswerEntity {
        $answerEntity = new AnswerEntity();

        $body           = $data['body'] ?? DataFactory::string('body');
        $channel        = $data['channel'] ?? 'bot';
        $questionEntity = $data['question'] ?? self::question(
            [
                'answers' => [
                    $answerEntity,
                ],
            ]
        );

        $answerEntity->setBody($body);
        $answerEntity->setChannel($channel);
        $answerEntity->setQuestion($questionEntity);

        return $answerEntity;
    }

    public static function question(
        array $data = []
    ): QuestionEntity {
        $questionEntity = new QuestionEntity();

        $answerEntities           = $data['answers'] ?? [
            self::answer(
                [
                    'question' => $questionEntity,
                ]
            ),
        ];
        $historicQuestionEntities = $data['historic_questions'] ?? [
            self::historicQuestion(
                [
                    'question' => $questionEntity,
                ]
            ),
        ];
        $promoted                 = $data['promoted'] ?? false;
        $status                   = $data['status'] ?? 'draft';
        $title                    = $data['title'] ?? DataFactory::string('title');

        foreach($answerEntities as $answerEntity) {
            $questionEntity->addAnswer($answerEntity);
        }
        foreach($historicQuestionEntities as $historicQuestionEntity) {
            $questionEntity->addHistoricQuestion($historicQuestionEntity);
        }
        $questionEntity->setPromoted($promoted);
        $questionEntity->setStatus($status);
        $questionEntity->setTitle($title);

        return $questionEntity;
    }
    
    public static function historicQuestion(
        array $data = []
    ): HistoricQuestionEntity {
        $historicQuestionEntity = new HistoricQuestionEntity();

        $field          = $data['field'] ?? DataFactory::string('field');
        $value          = $data['value'] ?? DataFactory::string('value');
        $questionEntity = $data['question'] ?? self::question(
            [
                'historic_questions' => [
                    $historicQuestionEntity,
                ],
            ]
        );

        $historicQuestionEntity->setField($field);
        $historicQuestionEntity->setValue($value);
        $historicQuestionEntity->setQuestion($questionEntity);

        return $historicQuestionEntity;
    }
}
