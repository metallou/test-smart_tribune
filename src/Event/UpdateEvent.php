<?php

namespace App\Event;

use App\Entity\QuestionEntity;

class UpdateEvent
{
    private string $field;
    private QuestionEntity $questionEntity;
    private string $value;

    public function __construct(
        string $field,
        QuestionEntity $questionEntity,
        string $value
    ) {
        $this->field          = $field;
        $this->questionEntity = $questionEntity;
        $this->value          = $value;
    }

    public function getField() : string
    {
        return $this->field;
    }

    public function getQuestionEntity() : QuestionEntity
    {
        return $this->questionEntity;
    }

    public function getValue() : string
    {
        return $this->value;
    }
}
