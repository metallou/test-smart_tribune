<?php

namespace App\Controller;

use App\Repository\HistoricQuestionEntityRepository;
use App\Repository\QuestionEntityRepository;
use App\Service\Controller\MainControllerService;
use App\Service\DataTransferObjectService;
use App\Service\ExportOperator;
use Doctrine\ORM\EntityManagerInterface;
use Psr\EventDispatcher\EventDispatcherInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    private MainControllerService $controllerService;

    public function __construct(
        MainControllerService $controllerService
    ) {
        $this->controllerService = $controllerService;
    }

    /**
     * @Route(
     *   methods = "POST",
     *   name = "questions_create",
     *   path = "/questions"
     * )
     */
    public function create(
        DataTransferObjectService $dataTransferObjectService,
        EntityManagerInterface $entityManager,
        Request $request
    ): JsonResponse {
        $data = $this->controllerService->create(
            $dataTransferObjectService,
            $entityManager,
            $request,
        );

        return $this->json(
            $data,
            201,
            [],
            [
                'groups' => [
                    'main',
                    'question',
                ],
            ]
        );
    }

    /**
     * @Route(
     *   methods = "PUT",
     *   name = "questions_update",
     *   path = "/questions/{id}",
     *   requirements={"id"="\d+"}
     * )
     */
    public function update(
        DataTransferObjectService $dataTransferObjectService,
        EntityManagerInterface $entityManager,
        EventDispatcherInterface $eventDispatcher,
        QuestionEntityRepository $questionEntityRepository,
        Request $request,
        int $id
    ): JsonResponse {
        $questionEntity = $questionEntityRepository->get($id);

        $data = $this->controllerService->update(
            $dataTransferObjectService,
            $entityManager,
            $eventDispatcher,
            $questionEntity,
            $request,
        );

        return $this->json(
            $data,
            200,
            [],
            [
                'groups' => [
                    'main',
                    'question',
                ],
            ]
        );
    }

    /**
     * @Route(
     *   methods = "GET",
     *   name = "histoc-question-create",
     *   path = "/historic-questions/export"
     * )
     */
    public function export(
        ExportOperator $exportOperator,
        HistoricQuestionEntityRepository $historicQuestionEntityRepository
    ): BinaryFileResponse {
        $file = $this->controllerService->export(
            $exportOperator,
            $historicQuestionEntityRepository,
        );

        return $this->file(
            $file,
            "historic-questions.csv",
        );
    }
}
