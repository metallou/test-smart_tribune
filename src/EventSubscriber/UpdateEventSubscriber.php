<?php

namespace App\EventSubscriber;

use App\Entity\HistoricQuestionEntity;
use App\Event\UpdateEvent;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class UpdateEventSubscriber implements EventSubscriberInterface
{
    public const CALLABLE = 'onUpdate';

    private EntityManagerInterface $entityManager;

    public function __construct(
        EntityManagerInterface $entityManager
    ) {
        $this->entityManager = $entityManager;
    }

    public static function getSubscribedEvents()
    {
        return [
            UpdateEvent::class => 'onUpdate',
        ];
    }

    public function onUpdate(
        UpdateEvent $updateEvent
    ) : UpdateEvent {
        $field          = $updateEvent->getField();
        $questionEntity = $updateEvent->getQuestionEntity();
        $value          = $updateEvent->getValue();

        $historicQuestionEntity = new HistoricQuestionEntity();

        $historicQuestionEntity->setQuestion($questionEntity);
        $historicQuestionEntity->setField($field);
        $historicQuestionEntity->setValue($value);

        $this->entityManager->persist($historicQuestionEntity);

        $this->entityManager->flush();

        return $updateEvent;
    }
}
