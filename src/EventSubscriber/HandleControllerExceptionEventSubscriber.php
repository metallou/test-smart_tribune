<?php

namespace App\EventSubscriber;

use App\Exception\ControllerException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Serializer\SerializerInterface;

class HandleControllerExceptionEventSubscriber implements EventSubscriberInterface
{
    public const CALLABLE = 'onException';

    private SerializerInterface $serializer;

    public function __construct(
        SerializerInterface $serializer
    ) {
        $this->serializer = $serializer;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::EXCEPTION => self::CALLABLE,
        ];
    }

    public function onException(
        ExceptionEvent $exceptionEvent
    ) : ExceptionEvent {
        $throwable = $exceptionEvent->getThrowable();

        if ($throwable instanceof ControllerException) {
            $data = $throwable->getData();

            $json = $this->serializer->serialize(
                $data,
                'json',
            );

            $response = new JsonResponse(
                $json,
                400,
                [],
                true,
            );

            $exceptionEvent->setResponse($response);
        }

        return $exceptionEvent;
    }
}