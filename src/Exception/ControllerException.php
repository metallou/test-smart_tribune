<?php

namespace App\Exception;

use ArrayObject;
use RuntimeException;

class ControllerException extends RuntimeException
{
    private ArrayObject $data;

    public function __construct(
        string $message,
        ArrayObject $data
    ) {
        parent::__construct(
            $message,
        );

        $this->data = $data;
    }

    public function getData(): ArrayObject
    {
        return $this->data;
    }
}
