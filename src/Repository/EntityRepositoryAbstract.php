<?php

namespace App\Repository;

use App\Entity\EntityAbstract;
use App\Entity\QuestionEntity;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityNotFoundException;
use Doctrine\Persistence\ManagerRegistry;
use function vsprintf;

/**
 * @method EntityAbstract|null find($id, $lockMode = null, $lockVersion = null)
 * @method EntityAbstract|null findOneBy(array $criteria, array $orderBy = null)
 * @method EntityAbstract[]    findAll()
 * @method EntityAbstract[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
abstract class EntityRepositoryAbstract extends ServiceEntityRepository
{
    public function get(
        $id,
        $lockMode = null,
        $lockVersion = null
    ): EntityAbstract {
        $entity = $this->find(
            $id,
            $lockMode,
            $lockVersion,
        );
        if (!$entity instanceof EntityAbstract) {
            $entityName = $this->getEntityName();
            
            $message = vsprintf(
                '%s(id=%s) not found',
                [
                    $entityName,
                    (string)$id
                ],
            );
            
            throw new EntityNotFoundException(
                $message
            );
        }

        return $entity;
    }
}
