<?php

namespace App\Repository;

use App\Entity\HistoricQuestionEntity;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method HistoricQuestionEntity      get($id, $lockMode = null, $lockVersion = null)
 * @method HistoricQuestionEntity|null find($id, $lockMode = null, $lockVersion = null)
 * @method HistoricQuestionEntity|null findOneBy(array $criteria, array $orderBy = null)
 * @method HistoricQuestionEntity[]    findAll()
 * @method HistoricQuestionEntity[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HistoricQuestionEntityRepository extends EntityRepositoryAbstract
{
    public function __construct(
        ManagerRegistry $registry
    ) {
        parent::__construct(
            $registry,
            HistoricQuestionEntity::class,
        );
    }
}
