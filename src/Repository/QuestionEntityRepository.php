<?php

namespace App\Repository;

use App\Entity\QuestionEntity;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method QuestionEntity      get($id, $lockMode = null, $lockVersion = null)
 * @method QuestionEntity|null find($id, $lockMode = null, $lockVersion = null)
 * @method QuestionEntity|null findOneBy(array $criteria, array $orderBy = null)
 * @method QuestionEntity[]    findAll()
 * @method QuestionEntity[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class QuestionEntityRepository extends EntityRepositoryAbstract
{
    public function __construct(
        ManagerRegistry $registry
    ) {
        parent::__construct(
            $registry,
            QuestionEntity::class,
        );
    }
}
