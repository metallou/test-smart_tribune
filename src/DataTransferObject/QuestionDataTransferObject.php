<?php

namespace App\DataTransferObject;

use Symfony\Component\Validator\Constraints;
use Symfony\Component\Validator\Mapping\ClassMetadata;

class QuestionDataTransferObject implements DataTransferObjectInterface
{
    public const STATUSES = [
        "draft",
        "published",
    ];
    public const ANSWER_CHANNELS = [
        "bot",
       "faq",
    ];

    /**
     * @Constraints\Collection(
     *   fields = {
     *     "answers" = {
     *       @Constraints\All(
     *         constraints = {
     *           @Constraints\Collection(
     *             fields = {
     *               "body" = {
     *                 @Constraints\Length(
     *                   max = 500
     *                 ),
     *                 @Constraints\NotBlank,
     *                 @Constraints\Type(
     *                   type = "string"
     *                 )
     *               },
     *               "channel" = {
     *                 @Constraints\Choice(
     *                   choices = QuestionDataTransferObject::ANSWER_CHANNELS
     *                 ),
     *                 @Constraints\NotNull
     *               }
     *             }
     *           ),
     *           @Constraints\NotNull
     *         }
     *       ),
     *       @Constraints\Count(
     *          min = 1
     *       ),
     *       @Constraints\NotNull,
     *       @Constraints\Type(
     *         type = "array"
     *       )
     *     },
     *     "promoted" = {
     *       @Constraints\NotNull,
     *       @Constraints\Type(
     *         type = "bool"
     *       )
     *     },
     *     "status" = {
     *       @Constraints\Choice(
     *         choices = QuestionDataTransferObject::STATUSES
     *       ),
     *       @Constraints\NotNull
     *     },
     *     "title" = {
     *       @Constraints\Length(
     *         max = 100
     *       ),
     *       @Constraints\NotBlank,
     *       @Constraints\Type(
     *         type = "string"
     *       )
     *     }
     *   }
     * )
     * @Constraints\NotNull
     */
    private array $data;
    /**
     * @Constraints\Json
     */
    private string $payload;

    public function __construct(
        string $payload
    ) {
        $this->payload = $payload;
    }

    public function getData(): array
    {
        return $this->data;
    }

    public function getPayload() : string
    {
        return $this->payload;
    }

    public function parsePayload() : void
    {
       $this->data = json_decode(
         $this->payload,
         true,
       ) ?? [];
    }
}