<?php

namespace App\Entity;

use App\Repository\HistoricQuestionEntityRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(
 *  repositoryClass=HistoricQuestionEntityRepository::class
 * )
 */
class HistoricQuestionEntity extends EntityAbstract
{
    /**
     * @ORM\Column(
     *   type="string",
     *   length=255
     * )
     * @Serializer\Groups({"main"})
     */
    private $field;

    /**
     * @ORM\Column(
     *   type="string",
     *   length=255
     * )
     * @Serializer\Groups({"main"})
     */
    private $value;

    /**
     * @ORM\ManyToOne(
     *   targetEntity=QuestionEntity::class,
     *   inversedBy="historicQuestions",
     *   cascade={"persist"}
     * )
     * @ORM\JoinColumn(
     *   nullable=false
     * )
     * @Serializer\Groups({"historic_question"})
     */
    private $question;

    public function getField(): ?string
    {
        return $this->field;
    }

    public function setField(
        string $field
    ): self {
        $this->field = $field;

        return $this;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(
        string $value
    ): self {
        $this->value = $value;

        return $this;
    }

    public function getQuestion(): ?QuestionEntity
    {
        return $this->question;
    }

    public function setQuestion(
        QuestionEntity $question
    ): self {
        $this->question = $question;

        return $this;
    }
}
