<?php

namespace App\Entity;

use App\Repository\QuestionEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(
 *   repositoryClass=QuestionEntityRepository::class
 * )
 * @ORM\HasLifecycleCallbacks
 */
class QuestionEntity extends EntityAbstract
{
    use TimestampedEntityTrait;

    /**
     * @ORM\Column(
     *   type="boolean"
     * )
     * @Serializer\Groups({"main"})
     */
    private ?bool $promoted = null;

    /**
     * @ORM\Column(
     *   type="string",
     *   length=9
     * )
     * @Serializer\Groups({"main"})
     */
    private ?string $status = null;

    /**
     * @ORM\Column(
     *   type="string",
     *   length=100
     * )
     * @Serializer\Groups({"main"})
     */
    private ?string $title = null;

    /**
     * @ORM\OneToMany(
     *   targetEntity=AnswerEntity::class,
     *   mappedBy="question",
     *   orphanRemoval=true,
     *   cascade={"persist", "remove"}
     * )
     * @Serializer\Groups({"question"})
     */
    private Collection $answers;

    /**
     * @ORM\OneToMany(
     *   targetEntity=HistoricQuestionEntity::class,
     *   mappedBy="question",
     *   orphanRemoval=true,
     *   cascade={"persist", "remove"}
     * )
     */
    private $historicQuestions;

    public function __construct()
    {
        $this->answers           = new ArrayCollection();
        $this->historicQuestions = new ArrayCollection();
    }

    public function isPromoted(): ?bool
    {
        return $this->promoted;
    }

    public function setPromoted(
        bool $promoted
    ): self {
        $this->promoted = $promoted;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(
        string $status
    ): self {
        $this->status = $status;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(
        string $title
    ): self {
        $this->title = $title;

        return $this;
    }

    /**
     * @return Collection|AnswerEntity[]
     */
    public function getAnswers(): Collection
    {
        return $this->answers;
    }

    public function addAnswer(
        AnswerEntity $answer
    ): self {
        if (!$this->answers->contains($answer)) {
            $this->answers[] = $answer;
            $answer->setQuestion($this);
        }

        return $this;
    }

    /**
     * @return Collection|HistoricQuestionEntity[]
     */
    public function getHistoricQuestions(): Collection
    {
        return $this->historicQuestions;
    }

    public function addHistoricQuestion(
        HistoricQuestionEntity $historicQuestion
    ): self {
        if (!$this->historicQuestions->contains($historicQuestion)) {
            $this->historicQuestions[] = $historicQuestion;
            $historicQuestion->setQuestion($this);
        }

        return $this;
    }
}
