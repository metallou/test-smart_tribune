<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use DateTime;
use DateTimeInterface;
use Symfony\Component\Serializer\Annotation as Serializer;

/**
 * @ORM\HasLifecycleCallbacks
 */
trait TimestampedEntityTrait
{
    /**
     * @ORM\Column(
     *   type="datetime"
     * )
     * @Serializer\Groups({"main"})
     */
    private $createdAt;

    /**
     * @ORM\Column(
     *   type="datetime"
     * )
     * @Serializer\Groups({"main"})
     */
    private $updatedAt;

    public function getCreatedAt(): ?DateTimeInterface
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): ?DateTimeInterface
    {
        return $this->updatedAt;
    }
    
    /**
    * @ORM\PrePersist
    * @ORM\PreUpdate
    */
    public function updateTimestamps(): void
    {
        $dateTime = new DateTime();

        $this->updatedAt = clone $dateTime;
        if (!$this->createdAt instanceof DateTimeInterface) {
            $this->createdAt = clone $dateTime;
        }
    }
}
