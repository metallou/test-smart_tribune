<?php

namespace App\Entity;

use App\Repository\QuestionEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation as Serializer;

/**
 * @ORM\MappedSuperclass
 */
abstract class EntityAbstract
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(
     *   type="integer"
     * )
     * @Serializer\Groups({"main"})
     */
    private ?int $id = null;

    public function getId(): ?int
    {
        return $this->id;
    }
}
