<?php

namespace App\Entity;

use App\Repository\AnswerEntityRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(
 *   repositoryClass=AnswerEntityRepository::class
 * )
 * @ORM\HasLifecycleCallbacks
 */
class AnswerEntity extends EntityAbstract
{
    use TimestampedEntityTrait;

    /**
     * @ORM\Column(
     *   type="string",
     *   length=500
     * )
     * @Serializer\Groups({"main"})
     */
    private ?string $body = null;

    /**
     * @ORM\Column(
     *   type="string",
     *   length=3
     * )
     * @Serializer\Groups({"main"})
     */
    private ?string $channel = null;

    /**
     * @ORM\ManyToOne(
     *   targetEntity=QuestionEntity::class,
     *   inversedBy="answers",
     *   cascade={"persist"}
     * )
     * @ORM\JoinColumn(
     *   nullable=false
     * )
     * @Serializer\Groups({"answer"})
     */
    private ?QuestionEntity $question = null;

    public function getBody(): ?string
    {
        return $this->body;
    }

    public function setBody(
        string $body
    ): self {
        $this->body = $body;

        return $this;
    }

    public function getChannel(): ?string
    {
        return $this->channel;
    }

    public function setChannel(
        string $channel
    ): self {
        $this->channel = $channel;

        return $this;
    }

    public function getQuestion(): ?QuestionEntity
    {
        return $this->question;
    }

    public function setQuestion(
        QuestionEntity $question
    ): self {
        $this->question = $question;

        return $this;
    }
}
