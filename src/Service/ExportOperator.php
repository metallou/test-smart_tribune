<?php

namespace App\Service;

use Doctrine\ORM\QueryBuilder;
use SplFileObject;
use Symfony\Component\Serializer\SerializerInterface;

class ExportOperator
{
    private SerializerInterface $serializer;

    public function __construct(
        SerializerInterface $serializer
    ) {
        $this->serializer = $serializer;
    }

    public function export(
        QueryBuilder $queryBuilder,
        array $options,
        callable $transformResult
    ) : string {
        $tempDir = sys_get_temp_dir();
        $tempName = tempnam(
            $tempDir,
            'export',
        );

        $file = new SplFileObject(
            $tempName,
            'wb',
        );

        $isFirstLinePrinted = false;

        $queryIterator = $queryBuilder
            ->getQuery()
            ->toIterable();
        foreach($queryIterator as $queryResult) {
            $serializerOptions = array_merge_recursive(
                $options,
                [
                    'no_headers'  => $isFirstLinePrinted,
                    'groups'      => [
                        'main',
                    ],
                ],
            );

            $result = $transformResult($queryResult);

            $serializedResult = $this->serializer->serialize(
                $result,
                'csv',
                $serializerOptions,
            );

            $isFirstLinePrinted = true;
            
            $file->fwrite($serializedResult);
        }

        return $file->getRealPath();
    }
}