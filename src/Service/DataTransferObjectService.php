<?php

namespace App\Service;

use App\DataTransferObject\DataTransferObjectInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class DataTransferObjectService
{
    private ValidatorInterface $validator;

    public function __construct(
        ValidatorInterface $validator
    ) {
        $this->validator = $validator;
    }

    public function validateProperty(
        DataTransferObjectInterface $dataTransferObject,
        string $property,
        $groups = null
    ) : ConstraintViolationListInterface {
        return $this->validator->validateProperty(
            $dataTransferObject,
            $property,
            $groups,
        );
    }
}