<?php

namespace App\Service\Controller;

use App\DataTransferObject\DataTransferObjectInterface;
use App\DataTransferObject\QuestionDataTransferObject;
use App\Entity\AnswerEntity;
use App\Entity\HistoricQuestionEntity;
use App\Entity\QuestionEntity;
use App\Event\UpdateEvent;
use App\Exception\ControllerException;
use App\Repository\HistoricQuestionEntityRepository;
use App\Service\DataTransferObjectService;
use App\Service\ExportOperator;
use ArrayObject;
use Doctrine\ORM\EntityManagerInterface;
use Psr\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class MainControllerService
{
    public function create(
        DataTransferObjectService $dataTransferObjectService,
        EntityManagerInterface $entityManager,
        Request $request
    ): ArrayObject {
        // validata data
        $questionData = $this->validateData(
            $dataTransferObjectService,
            $request,
            'create',
        );

        // persist data
        $questionEntity = new QuestionEntity();

        $questionEntity->setPromoted($questionData['promoted']);
        $questionEntity->setStatus($questionData['status']);
        $questionEntity->setTitle($questionData['title']);

        foreach($questionData['answers'] as $answerData) {
            $answerEntity = new AnswerEntity();

            $answerEntity->setBody($answerData['body']);
            $answerEntity->setChannel($answerData['channel']);

            $questionEntity->addAnswer($answerEntity);
        }

        $entityManager->persist($questionEntity);

        $entityManager->flush();

        return new ArrayObject(
            [
                'data' => $questionEntity,
            ]
        );
    }

    public function update(
        DataTransferObjectService $dataTransferObjectService,
        EntityManagerInterface $entityManager,
        EventDispatcherInterface $eventDispatcher,
        QuestionEntity $questionEntity,
        Request $request
    ): ArrayObject {
        // validata data
        $questionData = $this->validateData(
            $dataTransferObjectService,
            $request,
            'update',
        );

        // persist data
        $oldStatus = $questionEntity->getStatus();
        $oldTitle  = $questionEntity->getTitle();

        $newStatus = $questionData['status'];
        $newTitle  = $questionData['title'];

        $questionEntity->setStatus($newStatus);
        $questionEntity->setTitle($newTitle);
        
        $entityManager->persist($questionEntity);

        $entityManager->flush();

        if ($oldStatus !== $newStatus) {
            $updateEvent = new UpdateEvent(
                'status',
                $questionEntity,
                $oldStatus,
            );

            $eventDispatcher->dispatch($updateEvent);
        }
        if ($oldTitle !== $newTitle) {
            $updateEvent = new UpdateEvent(
                'title',
                $questionEntity,
                $oldTitle,
            );

            $eventDispatcher->dispatch($updateEvent);
        }

        return new ArrayObject(
            [
                'data' => $questionEntity,
            ]
        );
    }

    private function validateData(
        DataTransferObjectService $dataTransferObjectService,
        Request $request,
        string $group
    ) : array {
        $body = $request->getContent() ?? '';

        $questionDataTransferObject = new QuestionDataTransferObject($body);

        $this->validateDataProperty(
            $dataTransferObjectService,
            $questionDataTransferObject,
            'payload',
            $group,
            'invalid payload',
        );

        $questionDataTransferObject->parsePayload();

        $this->validateDataProperty(
            $dataTransferObjectService,
            $questionDataTransferObject,
            'data',
            $group,
            'invalid data',
        );
        
        return $questionDataTransferObject->getData();
    }

    private function validateDataProperty(
        DataTransferObjectService $dataTransferObjectService,
        DataTransferObjectInterface $dataTransferObject,
        string $property,
        string $group,
        string $message
    ) : void {
        $groups = [
            'Default',
            $group,
        ];

        $violations = $dataTransferObjectService->validateProperty(
            $dataTransferObject,
            $property,
            $groups,
        );
        if (count($violations) > 0) {
            $data = new ArrayObject(
                [
                    'violations' => $violations,
                ]
            );

            throw new ControllerException(
                $message,
                $data,
            );
        }   
    }

    public function export(
        ExportOperator $exportOperator,
        HistoricQuestionEntityRepository $historicQuestionEntityRepository
    ) : string {
        $queryBuilder = $historicQuestionEntityRepository
            ->createQueryBuilder('HistoricQuestion')
            ->orderBy('HistoricQuestion.question');

        return $exportOperator->export(
            $queryBuilder,
            [],
            static function(
                HistoricQuestionEntity $historicQuestionEntity
            ) : array {
                $questionEntity = $historicQuestionEntity->getQuestion();

                return [
                    'id'          => $historicQuestionEntity->getId(),
                    'question_id' => $questionEntity->getId(),
                    'field'       => $historicQuestionEntity->getField(),
                    'value'       => $historicQuestionEntity->getValue(),
                ];
            }
        );
    }
}
